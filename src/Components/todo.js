import React from 'react';

import { Checkbox, Button } from 'semantic-ui-react'

class Todo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            checked : false
                }

        this.handleClick = this.handleClick.bind(this)
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
    }

    handleCheckboxChange = (event) => {
        // alert('youpi!')
        console.log('hey')
        this.setState({ checked: event.target.checked});
        this.props.onCheck(event.target.checked, this.props.id, this.props.text);
       
    }

    handleClick = () => {
        this.props.onDeletion(this.props.id)
    }

    render() {

       
        return (
            <div>
                <Checkbox
                    name="checked"
                    label={this.props.text}
                    checked={this.props.checked}
                    onChange={this.handleCheckboxChange}
                />
                <Button onClick={this.handleClick}>X</Button>
            </div>
        );
    }
}
export default Todo;