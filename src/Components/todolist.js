import React from 'react';

import Todo from './todo';
import InputTodo from './inputTodo'

class TodoList extends React.Component {

    constructor(props) {
        super(props);
        //On fixe les valeurs des attributs du state 
        this.state = {
            list: [],
            doneList: [],
            dataInput: '',
        };
        this.addTodo = this.addTodo.bind(this)
        this.deleteTodo = this.deleteTodo.bind(this)
        this.checkTodo = this.checkTodo.bind(this)
        this.addToDoneList = this.addToDoneList.bind(this)
    }
    //on passe en paramètres les données de l'enfant que l'on veut faire remonter 
    addTodo(title) {
        let list = [...this.state.list];
        //définition des props pour être appelées dans le render de Todo : 
        list.push(<li
            text={title} key={list.length} id={list.length}
            onDeletion={this.deleteTodo}
            onCheck={this.checkTodo}
        />);
        this.setState({ list }, {title});
    }

    deleteTodo(id) {
        let list = [...this.state.list];
        let index = id;
        list.splice(index, 1);
        //ne lance rien tant que la 1ère étape n'est pas exécutée, 
        // quand on veut être sûr que les changements ont bien été pris en compte pour la suite, on lit les 2 dans le setState
        this.setState({ list }, () => {
            let list = [...this.state.list]
                      
            for (let i = 0; i < list.length; i++) {
                let text = list[i].props.text;
                list[i] = <Todo text={text} key={i} id={i}
                    onDeletion={this.deleteTodo}
                    onCheck={this.checkTodo} />
            }
            this.setState({ list });
        });
    };

    addToDoneList(title) {
        console.log("coucou")
        let doneList = [...this.state.doneList]
        doneList.push(<Todo
            text={title} key={doneList.length} id={doneList.length}
            checked={true}
            onDeletion={this.deleteTodo}
        />);
        this.setState({ doneList });
    }
 
    checkTodo(checked, id, title) {
        if (checked === true) {
            // alert("Checked")
            this.addToDoneList(title)
            this.deleteTodo(id)
        } else {
            alert("Unchecked")
        }
       
       
    }

    render() {
        return (
            <div>
                {this.state.list.length}
                <InputTodo onValidation={this.addTodo} />
                <h2>A faire</h2>
                {this.state.list}
                <h2>Fait</h2>
                {this.state.doneList}
                {this.state.list.t}

            </div>
        );
    }
}
//ne pas mettre des arrow functions dans le render (on peut les appeler bien sûr mais pas les créer)

export default TodoList;


