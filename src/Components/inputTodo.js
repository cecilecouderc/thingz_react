import React from 'react';

import {
    Button,
    Input
} from 'semantic-ui-react';

class InputTodo extends React.Component {

    constructor() {
        super()
        this.state = {
            dataInput: ''
        }
        this.onChange = this.onChange.bind(this)
    }

     //donne la valeur de l'input au state ;  
    onChange = (event) => {
        this.setState({
            dataInput: event.target.value
        });
    }
    // fonction callback rappel de la fonction onClick
    addTodo = () => { this.props.onValidation(this.state.dataInput) };
    
    
    render() {

        return (
            <div>
                <Input
                    type="text"
                    placeholder='Entrer une nouvelle tâche'
                    value={this.state.dataInput}
                    onChange={this.onChange} />
                <Button onClick={this.addTodo}>OK</Button>
            </div>
        )
    }
}

export default InputTodo;